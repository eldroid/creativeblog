package com.eldroid.blogdemo;

import com.eldroid.blogdemo.models.Blog;
import com.eldroid.blogdemo.presenters.MainPresenter;
import com.eldroid.blogdemo.repositories.BlogRepository;
import com.eldroid.blogdemo.utils.BlogCallback;
import com.eldroid.blogdemo.utils.BlogListCallback;
import com.eldroid.blogdemo.utils.ConnManager;
import com.eldroid.blogdemo.views.BlogListView;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Vujica on 03-Mar-17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {
    @Captor
    ArgumentCaptor<BlogListCallback> blogCallbackArgumentCaptor;
    @Captor
    ArgumentCaptor<BlogCallback>singleblogCallbackArgumentCaptor;
@Mock
    BlogListView view;
    @Mock
    BlogRepository blogRepository;
    @Mock
    ConnManager connManager;
    private MainPresenter presenter;

    @Test public void shouldPAss()
    {
        Assert.assertEquals(true,2+4==6);
    }

    @Test public void shouldCallGetBooksFromModel()
    {
        setUpPresenter();
        presenter.displayBlogList();
        Mockito.verify(blogRepository).getBlogs(blogCallbackArgumentCaptor.capture());
    }
    @Test public void shouldDisplayOnSuccess()
    {
        List<Blog> blogList=Arrays.asList(new Blog(),new Blog(),new Blog());
       setUpPresenter();
        presenter.displayBlogList();
        Mockito.verify(blogRepository).getBlogs(blogCallbackArgumentCaptor.capture());
        blogCallbackArgumentCaptor.getValue().onSuccess(blogList);
        Mockito.verify(view).displayBlogList(blogList);

    }
    @Test public void shouldDisplayError()
    {
       setUpPresenter();
        presenter.displayBlogList();
        Mockito.verify(blogRepository).getBlogs(blogCallbackArgumentCaptor.capture());
        blogCallbackArgumentCaptor.getValue().onError(401);
        Mockito.verify(view).handleError(401);
    }
    @Test public void shouldDisplayBlog()
    {

        setUpPresenter();
        presenter.displayBlog(4);
        Mockito.verify(blogRepository).getBlog(Mockito.anyInt(),singleblogCallbackArgumentCaptor.capture());
        singleblogCallbackArgumentCaptor.getValue().onSuccess(new Blog());
        Mockito.verify(view).displayBlog(Mockito.any(Blog.class));
    }
    @Test public void shoulDisplayErrorGettingBlog()
    {
        setUpPresenter();
        presenter.displayBlog(4);
        Mockito.verify(blogRepository).getBlog(Mockito.anyInt(),singleblogCallbackArgumentCaptor.capture());
        singleblogCallbackArgumentCaptor.getValue().onError(401);
        Mockito.verify(view).handleError(401);
    }
    @Test public void shouldDisplayNoInternetDialog()
    {
       setUpPresenterNoConnectivity();
        //when
        presenter.displayBlogList();
        Mockito.verify(view).displayNoInternetError();

    }
    @Test public void shouldDisplayNoInternetWhenBlogClicked()
    {
        //given
       setUpPresenterNoConnectivity();
        //when
        presenter.displayBlog(1);
        Mockito.verify(view).displayNoInternetError();
    }
    public void setUpPresenter()
    {
        //given
        Mockito.when(connManager.isConnected()).thenReturn(true);
        presenter = new MainPresenter(view,blogRepository,connManager);

    }
    public void setUpPresenterNoConnectivity()
    {
        //given
        Mockito.when(connManager.isConnected()).thenReturn(false);
        presenter=new MainPresenter(view,blogRepository,connManager);
    }
}
