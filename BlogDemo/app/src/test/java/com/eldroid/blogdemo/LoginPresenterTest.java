package com.eldroid.blogdemo;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.eldroid.blogdemo.activities.LoginActivity;
import com.eldroid.blogdemo.application.BlogApplication;
import com.eldroid.blogdemo.models.UserRepository;
import com.eldroid.blogdemo.models.User;
import com.eldroid.blogdemo.models.UserBuilder;
import com.eldroid.blogdemo.presenters.LoginPresenter;
import com.eldroid.blogdemo.utils.ConnManager;
import com.eldroid.blogdemo.utils.UserLoginCallback;
import com.eldroid.blogdemo.views.LoginView;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.regex.Matcher;

/**
 * Created by Vujica on 02-Mar-17.
 */

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {
    @Mock LoginActivity activity;
    @Mock TextUtils textUtils;
    @Mock LoginView view;
    @Captor ArgumentCaptor <UserLoginCallback> userLoginCallbackArgumentCaptor;
    @Mock UserRepository model;
    @Mock ConnManager connManager;
    @Mock AutoCompleteTextView autocomplete_email;
    @Mock EditText edt_pass;
   @Test public void shouldPass()
    {
        Assert.assertEquals(true,1+1==2);
    }
    @Test public void shouldStartMAinActivity()
    {
        User user=new UserBuilder().setEmail("neko@imajl.com").setToken("198923fasd32").createUser();
        Mockito.when(model.getUser()).thenReturn(user);
        LoginPresenter presenter=new LoginPresenter(view,model);
        presenter.Login();
        Mockito.verify(view).startMainActivity();
    }

    @Test public void shouldDisplayLoginFields()
    {
        Mockito.when(model.getUser()).thenReturn(null);
        LoginPresenter presenter=new LoginPresenter(view,model);
        presenter.Login();
        Mockito.verify(view).showLoginFields();
    }


    @Test public void shouldShowNoEmailError()
    {
        Mockito.when(autocomplete_email.length()).thenReturn(0);
        LoginPresenter presenter=new LoginPresenter(view,model);
        Mockito.when(edt_pass.getText()).thenReturn(new MockEditable("121321"));
        presenter.validateInput(autocomplete_email,edt_pass);
        Mockito.verify(view).showNoEmailError();
    }
    @Test public void shouldShowEmailFormatError()
    {
        String pass="123456";
      //  Mockito.when(autocomplete_email.getText()).thenReturn(new MockEditable("qweqwe@AQ"));
        Mockito.when(autocomplete_email.length()).thenReturn(7);
        Mockito.when(edt_pass.getText()).thenReturn(new MockEditable(pass));
        MockPresenter mockPresenter=new MockPresenter(view,model,true,false);
        mockPresenter.validateInput(autocomplete_email,edt_pass);
        Mockito.verify(view).showEmailError();

    }
    @Test public void shouldShowPasswordError()
    {
        //Mockito.when(autocomplete_email.getText()).thenReturn(new MockEditable("asdasd@asdad.com"));
        Mockito.when(autocomplete_email.length()).thenReturn(7);
        String pass="1234";
        MockPresenter presenter=new MockPresenter(view,model,true,false);
        //when
        Mockito.when(edt_pass.getText()).thenReturn(new MockEditable(pass));


        Assert.assertEquals(false,presenter.validateInput(autocomplete_email,edt_pass));
        Mockito.verify(view).showPasswordError();

    }
    @Test public void shouldPassValuesToModel()
    {
        Mockito.when(connManager.isConnected()).thenReturn(true);
        String email="hello.asd@asd.com";
        String pass="1234asdaf";
        Mockito.when(autocomplete_email.getText()).thenReturn(new MockEditable(email));
        Mockito.when(edt_pass.getText()).thenReturn(new MockEditable(pass));
        LoginPresenter presenter=new LoginPresenter(view,model,connManager);
        presenter.loginUser(autocomplete_email,edt_pass);
        Mockito.verify(model).login(Mockito.eq(email),Mockito.eq(pass),userLoginCallbackArgumentCaptor.capture());
        userLoginCallbackArgumentCaptor.getValue().onLoginSuccess("anuthing");

    }
    @Test public void shouldShowLoginError()
    {
        Mockito.when(connManager.isConnected()).thenReturn(true);
        String email="hello.asd@asd.com";
        String pass="wrong_pass";
        Mockito.when(autocomplete_email.getText()).thenReturn(new MockEditable(email));
        Mockito.when(edt_pass.getText()).thenReturn(new MockEditable(pass));
        LoginPresenter presenter=new LoginPresenter(view,model,connManager);
        presenter.loginUser(autocomplete_email,edt_pass);
        Mockito.verify(model).login(Mockito.eq(email),Mockito.eq(pass),userLoginCallbackArgumentCaptor.capture());
        userLoginCallbackArgumentCaptor.getValue().onLoginError(401);
        Mockito.verify(view).displayLoginError(401);
    }
    @Test public void shouldShowNoInternetConnection()
    {
        Mockito.when(connManager.isConnected()).thenReturn(false);
        String email="hello.asd@asd.com";
        String pass="good_pass";
//        Mockito.when(autocomplete_email.getText()).thenReturn(new MockEditable(email));
//        Mockito.when(edt_pass.getText()).thenReturn(new MockEditable(pass));
        LoginPresenter presenter=new LoginPresenter(view,model,connManager);
        presenter.loginUser(autocomplete_email,edt_pass);
        Mockito.verify(view).showNoConnectivity();

    }
    class MockEditable implements Editable {

        private String str;
        public MockEditable(String str) {
            this.str = str;
        }

        @Override @NonNull
        public String toString() {
            return str;
        }

        @Override
        public int length() {
            return str.length();
        }

        @Override
        public char charAt(int i) {
            return str.charAt(i);
        }

        @Override
        public CharSequence subSequence(int i, int i1) {
            return str.subSequence(i, i1);
        }

        @Override
        public Editable replace(int i, int i1, CharSequence charSequence, int i2, int i3) {
            return this;
        }

        @Override
        public Editable replace(int i, int i1, CharSequence charSequence) {
            return this;
        }

        @Override
        public Editable insert(int i, CharSequence charSequence, int i1, int i2) {
            return this;
        }

        @Override
        public Editable insert(int i, CharSequence charSequence) {
            return this;
        }

        @Override
        public Editable delete(int i, int i1) {
            return this;
        }

        @Override
        public Editable append(CharSequence charSequence) {
            return this;
        }

        @Override
        public Editable append(CharSequence charSequence, int i, int i1) {
            return this;
        }

        @Override
        public Editable append(char c) {
            return this;
        }

        @Override
        public void clear() {
        }

        @Override
        public void clearSpans() {
        }

        @Override
        public void setFilters(InputFilter[] inputFilters) {
        }

        @Override
        public InputFilter[] getFilters() {
            return new InputFilter[0];
        }

        @Override
        public void getChars(int i, int i1, char[] chars, int i2) {
        }

        @Override
        public void setSpan(Object o, int i, int i1, int i2) {
        }

        @Override
        public void removeSpan(Object o) {
        }

        @Override
        public <T> T[] getSpans(int i, int i1, Class<T> aClass) {
            return null;
        }

        @Override
        public int getSpanStart(Object o) {
            return 0;
        }

        @Override
        public int getSpanEnd(Object o) {
            return 0;
        }

        @Override
        public int getSpanFlags(Object o) {
            return 0;
        }

        @Override
        public int nextSpanTransition(int i, int i1, Class aClass) {
            return 0;
        }
    }
    class MockPresenter extends LoginPresenter{
        private LoginView view;
        private boolean wrongemail;
        private boolean wrongpass;
        public MockPresenter(LoginView view, UserRepository logmodel) {
            super(view, logmodel);
        }
        public MockPresenter (LoginView view,UserRepository userRepository,boolean wrongEmail,boolean wrongPass)
        {
            super(view, userRepository);
            this.view=view;
            this.wrongemail=wrongEmail;
            this.wrongpass=wrongPass;
        }

        @Override
        public boolean validateInput(AutoCompleteTextView email, EditText password) {
            boolean input_valid=false;
            if(email.length()==0)
            {
                view.showNoEmailError();
            }
            else
            if(wrongemail) {
                view.showEmailError();
            }
            else
                input_valid=true;
            if(password.getText().length()<6) {
                view.showPasswordError();
                input_valid=false;
            }
            return input_valid;

        }
    }
}
