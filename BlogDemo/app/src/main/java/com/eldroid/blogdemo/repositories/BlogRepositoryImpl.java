package com.eldroid.blogdemo.repositories;
import android.content.Context;
import android.content.SharedPreferences;

import com.eldroid.blogdemo.application.BlogApplication;
import com.eldroid.blogdemo.models.Blog;
import com.eldroid.blogdemo.responses.BlogResponse;
import com.eldroid.blogdemo.utils.ApiError;
import com.eldroid.blogdemo.utils.BlogCallback;
import com.eldroid.blogdemo.utils.BlogListCallback;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Vujica on 03-Mar-17.
 */

public class BlogRepositoryImpl implements BlogRepository {
private Context context;
    private SharedPreferences sharedPreferences;
    private String token;
    public BlogRepositoryImpl(Context context)
    {
        this.context=context;
        sharedPreferences=context.getSharedPreferences(BlogApplication.PREFER_NAME,1);
        token=sharedPreferences.getString("token",null);
    }

    @Override
    public void getBlogs(final BlogListCallback callback) {
        //get blogs from service

        Call<List<Blog>> getBlogsCall= BlogApplication.getBLogService().getBlogList(token);
        getBlogsCall.enqueue(new Callback<List<Blog>>() {
            @Override
            public void onResponse(Call<List<Blog>> call, Response<List<Blog>> response) {
                if(response.code()==ApiError.OK)
                {
                    List<Blog>blogList= response.body();
                    if(blogList!=null)
                        callback.onSuccess(blogList);
                    else
                        callback.onError(ApiError.SOMETHING_WENT_WRONG);
                }
                else
                    callback.onError(response.code());
            }

            @Override
            public void onFailure(Call<List<Blog>> call, Throwable t) {
                   callback.onError(ApiError.SOMETHING_WENT_WRONG);
            }
        });

    }
    @Override
    public void deleteToken()
    {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("token",null);
        editor.commit();
    }

    @Override
    public void getBlog(int id, final BlogCallback callback) {
        final Blog blog=new Blog();
        Call<BlogResponse> blogCall=BlogApplication.getBLogService().getBlog(id,token);
        blogCall.enqueue(new Callback<BlogResponse>() {
            @Override
            public void onResponse(Call<BlogResponse> call, Response<BlogResponse> response) {
                if(response.code()==200)
                {
                    blog.setDescription(response.body().getContent());
                    callback.onSuccess(blog);
                }
                else
                    callback.onError(response.code());
            }

            @Override
            public void onFailure(Call<BlogResponse> call, Throwable t) {
                    callback.onError(ApiError.SOMETHING_WENT_WRONG);
            }
        });
    }
}
