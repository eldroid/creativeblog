package com.eldroid.blogdemo.fragments;

/**
 * Created by Vujica on 05-Mar-17.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eldroid.blogdemo.activities.MainActivity;


public abstract class BaseFragment extends Fragment {
    protected MainActivity mainActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResId(), container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        mainActivity = (MainActivity) getActivity();
        linkUI();
        init();
        setData();
        setActions();
    }

    abstract protected int getLayoutResId();
    protected void linkUI() {}
    protected void setActions() {}
    public void init() {}
    public void setData() {}
    public void clearData(){}

    @Override
    public void onDestroyView() {
        try{
            clearData();
        }catch (Exception e)
        {
            e.toString();
        }
        super.onDestroyView();
    }
}

