package com.eldroid.blogdemo.activities;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.eldroid.blogdemo.R;
import com.eldroid.blogdemo.adapters.BlogListAdapter;
import com.eldroid.blogdemo.fragments.BlogFragment;
import com.eldroid.blogdemo.models.Blog;
import com.eldroid.blogdemo.presenters.MainPresenter;
import com.eldroid.blogdemo.repositories.BlogRepositoryImpl;
import com.eldroid.blogdemo.utils.OnBlogClickListener;
import com.eldroid.blogdemo.views.BlogListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by Vujica on 01-Mar-17.
 */

public class MainActivity extends BaseActivity implements BlogListView {
    LinearLayoutManager linearLayoutManager;
    RecyclerView rv_blogList;
    ProgressBar progressBar;
    private MainPresenter presenter;
    private BlogRepositoryImpl blogRepository;
    private ArrayList<Blog> blogArrayList;
    private BlogListAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected int getLayoutResID() {
        return R.layout.activity_main;
    }



    @Override
    protected void linkUI() {
        rv_blogList=(RecyclerView)findViewById(R.id.rv_blogList);
        progressBar=(ProgressBar)findViewById(R.id.progress_blog);
        swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.refresh_Layout);
        linearLayoutManager=new LinearLayoutManager(this);
        rv_blogList.setLayoutManager(linearLayoutManager);
        rv_blogList.setFitsSystemWindows(true);
        if(getSupportActionBar()!=null)
        getSupportActionBar().setTitle("Blog List");
    }

    @Override
    protected void init() {
        blogArrayList=new ArrayList<>();
        blogRepository = new BlogRepositoryImpl(MainActivity.this);
        presenter = new MainPresenter(this, blogRepository);
        adapter = new BlogListAdapter(this, blogArrayList, new OnBlogClickListener() {
            @Override
            public void onBlogClicked(Blog blog) {
                presenter.displayBlog(blog.getId());
            }
        });
        rv_blogList.setAdapter(adapter);
    }

    @Override
    protected void setActionBarTitle() {
        if(getSupportActionBar()!=null)
            getSupportActionBar().setTitle("Blog List");
    }

    @Override
    protected void setData() {
        presenter.displayBlogList();
    }

    @Override
    protected void setActions() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.clear();
                presenter.displayBlogList();
            }
        });
    }

    @Override
    public void displayBlogList(List<Blog> blogList) {
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        adapter.clear();
        adapter.addAll(new ArrayList<Blog>(blogList));
    }

    @Override
    public void displayNoInternetError() {
        new AlertDialog.Builder(this)
                .setTitle("Not connected")
                .setMessage("Please turn on internet connection !!!")
                .setCancelable(false)
                .setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                })
                .show();

    }

    @Override
    public void handleError(int errorNum) {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void logout() {
        Intent intent=new Intent(MainActivity.this,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void displayBlog(Blog blog) {
        Bundle bundle=new Bundle();
        BlogFragment blogFragment=new BlogFragment();
        bundle.putSerializable(Blog.KEY, blog);
        blogFragment.setArguments(bundle);
        this.addFragment(R.id.rel_main_view,blogFragment,BlogFragment.TAG);
    }

    @Override
    public Context getAppContext() {
        return this.getApplicationContext();
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("fragments",mStack);
    }
}
