package com.eldroid.blogdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.eldroid.blogdemo.R;
import com.eldroid.blogdemo.presenters.LoginPresenter;
import com.eldroid.blogdemo.repositories.UserRepo;
import com.eldroid.blogdemo.utils.ApiError;
import com.eldroid.blogdemo.views.LoginView;

/**
 * Created by Vujica on 01-Mar-17.
 */

public class LoginActivity extends BaseActivity implements LoginView {
    private static String TAG=LoginActivity.class.getSimpleName();
    private AutoCompleteTextView actv_email;
    private EditText edt_password;
    private Button btn_signIn;
    private ScrollView login_lay;
    private ProgressBar progressBar;
    private LinearLayout layout_wrapper;
    private LoginPresenter presenter;



    @Override
    protected int getLayoutResID() {
        return R.layout.activity_login;
    }

    @Override
    protected void linkUI() {
        actv_email=(AutoCompleteTextView)findViewById(R.id.email);
        edt_password=(EditText)findViewById(R.id.password);
        btn_signIn=(Button)findViewById(R.id.email_sign_in_button);
        login_lay=(ScrollView)findViewById(R.id.login_form);
        progressBar=(ProgressBar)findViewById(R.id.login_progress);
        layout_wrapper = (LinearLayout)findViewById(R.id.layout_wrap);

    }

    @Override
    protected void init() {
        login_lay.setVisibility(View.GONE);
        UserRepo repo=new UserRepo(this);
        presenter = new LoginPresenter(this,repo);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setTitle("Login");


    }

    @Override
    protected void setData() {
        presenter.Login();
    }

    @Override
    protected void setActions() {
        btn_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
if(presenter.validateInput(actv_email,edt_password))
    presenter.loginUser(actv_email,edt_password);
            }
        });
    }

    @Override
    public void displayLoginError(int ErrorType) {
        switch (ErrorType)
        {
            case ApiError.BAD_REQUEST:
                Snackbar.make(layout_wrapper,R.string.error_bad_request,Snackbar.LENGTH_LONG).show();
                break;
            case ApiError.MEDIA_HEADER_WRONG:
                Snackbar.make(layout_wrapper,R.string.error_bad_media_type,Snackbar.LENGTH_LONG).show();
                break;
            case ApiError.WRONG_CONTENT_TYPE:
                Snackbar.make(layout_wrapper,R.string.error_bad_media_type,Snackbar.LENGTH_LONG).show();
                break;
            case ApiError.WRONG_CREDENTIALS:
                Snackbar.make(layout_wrapper,R.string.error_wrong_credentials,Snackbar.LENGTH_LONG).show();
                break;
            default:
                Snackbar.make(layout_wrapper,R.string.error_login,Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public void startMainActivity() {
        Intent intent=new Intent(LoginActivity.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(intent);
        finish();
        Log.d(TAG,"Starting Main Activity");
    }

    @Override
    public void showLoginFields() {
        progressBar.setVisibility(View.GONE);
        login_lay.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmailError() {
            actv_email.setError("Please input valid email ");
    }

    @Override
    public void showNoEmailError() {
            actv_email.setError("Enter email");
    }

    @Override
    public void showPasswordError() {
            edt_password.setError("Password should be al least 6 characters");
    }

    @Override
    public void showNoConnectivity() {
        Snackbar.make(layout_wrapper,R.string.error_no_connection,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public Context getAppContext() {
        return this.getApplicationContext();
    }


}
