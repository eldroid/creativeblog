package com.eldroid.blogdemo.utils;

import com.eldroid.blogdemo.models.Blog;

import java.util.List;

/**
 * Created by Vujica on 03-Mar-17.
 */

public interface BlogListCallback {
    public void onSuccess(List<Blog> bloglist);
    public void onError(int errorCode);

}
