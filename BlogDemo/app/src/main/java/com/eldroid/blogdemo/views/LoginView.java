package com.eldroid.blogdemo.views;

import android.content.Context;

/**
 * Created by Vujica on 02-Mar-17.
 */

public interface LoginView {
    public void displayLoginError(int error);
    public void startMainActivity();
    public void showLoginFields();
    public void showNoEmailError();
    public void showEmailError();
    public void showPasswordError();
    public void showNoConnectivity();
    public Context getAppContext();

}
