package com.eldroid.blogdemo.models;


public class UserBuilder {
    private String email;
    private String token="";
    private String pass;

    public UserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder setToken(String token) {
        this.token = token;
        return this;
    }

    public UserBuilder setPassword(String Pass) {
        this.pass=Pass;
    return this;
}

    public User createUser() {
        return new User(email, token,pass);
    }
}