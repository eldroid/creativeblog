package com.eldroid.blogdemo.presenters;

import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.eldroid.blogdemo.R;
import com.eldroid.blogdemo.application.BlogApplication;
import com.eldroid.blogdemo.models.UserRepository;
import com.eldroid.blogdemo.models.User;
import com.eldroid.blogdemo.utils.ConnManager;
import com.eldroid.blogdemo.utils.UserLoginCallback;
import com.eldroid.blogdemo.views.LoginPresenterOps;
import com.eldroid.blogdemo.views.LoginView;

/**
 * Created by Vujica on 02-Mar-17.
 */

public class LoginPresenter implements LoginPresenterOps {

    private final UserRepository model;
    private final LoginView loginView;
    private ConnManager connManager;



    public LoginPresenter(LoginView view, UserRepository logmodel) {
        loginView = view;
        model = logmodel;
        connManager=ConnManager.getInstance(view.getAppContext());
    }

    //for testing
    public LoginPresenter(LoginView view,UserRepository logmodel,ConnManager manager)
    {
        loginView = view;
        model = logmodel;
        connManager=manager;
    }
    public void Login()
    {
        User user=model.getUser();
        if(user!=null && user.getToken().length()>5) {
            loginView.startMainActivity();
        }
        else{
            loginView.showLoginFields();
        }
    }

    @Override
    public void loginUser(AutoCompleteTextView email, EditText password) {

        if(connManager.isConnected())
        model.login(email.getText().toString(), password.getText().toString(), new UserLoginCallback() {
            @Override
            public void onLoginSuccess(String token) {
                model.saveUserCredentials(token);
                Login();
            }

            @Override
            public void onLoginError(int number) {
                loginView.displayLoginError(number);
            }
        });
        else
            loginView.showNoConnectivity();



    }
    public boolean validateInput(AutoCompleteTextView email, EditText password)
    {
        boolean input_valid=false;
        if(email.length()==0)
        {
            loginView.showNoEmailError();
        }
        else
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            loginView.showEmailError();
        }
        else
        input_valid=true;
        if(password.getText().length()<6) {
            loginView.showPasswordError();
            input_valid=false;
        }
        return input_valid;
    }
}
