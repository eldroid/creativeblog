package com.eldroid.blogdemo.presenters;

import com.eldroid.blogdemo.models.Blog;
import com.eldroid.blogdemo.repositories.BlogRepository;
import com.eldroid.blogdemo.utils.ApiError;
import com.eldroid.blogdemo.utils.BlogCallback;
import com.eldroid.blogdemo.utils.BlogListCallback;
import com.eldroid.blogdemo.utils.ConnManager;
import com.eldroid.blogdemo.views.BlogListView;

import java.util.List;

/**
 * Created by Vujica on 03-Mar-17.
 */

public class MainPresenter {
    private BlogListView view;
    private BlogRepository blogRepository;
    private ConnManager connManager;

    public MainPresenter(BlogListView blogListView,BlogRepository blog)
    {
        view=blogListView;
        blogRepository=blog;
        connManager=ConnManager.getInstance(view.getAppContext());
    }

    //for tests
    public MainPresenter(BlogListView blogListView,BlogRepository blog,ConnManager manager)
    {
        view=blogListView;
        blogRepository=blog;
        connManager=manager;
    }

    public void displayBlogList()
    {
        if(connManager.isConnected())
        blogRepository.getBlogs(new BlogListCallback() {
            @Override
            public void onSuccess(List<Blog> bloglist) {
                //we have blogs from blog repo, send them to view
                view.displayBlogList(bloglist);
            }

            @Override
            public void onError(int errorCode) {
                //some error ocured while trying to retrieve blogs
                //handle error
                view.handleError(errorCode);
                switch (errorCode){
                    case ApiError.WRONG_CREDENTIALS:
                        Logout();
                        break;
                    case  ApiError.BAD_REQUEST:
                        view.handleError(ApiError.BAD_REQUEST);
                        break;
                    default:
                        view.handleError(ApiError.SOMETHING_WENT_WRONG);
                }
            }
        });
        else
            view.displayNoInternetError();

    }
    public void displayBlog(int id)
    {
        if(connManager.isConnected())
            blogRepository.getBlog(id, new BlogCallback() {
                @Override
                public void onSuccess(Blog blog) {
                    view.displayBlog(blog);
                }

                @Override
                public void onError(int errorCode) {
                    view.handleError(errorCode);
                }
            });
        else
            view.displayNoInternetError();
    }
    public void Logout()
    {
        blogRepository.deleteToken();
        view.logout();
    }

}
