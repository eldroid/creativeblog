package com.eldroid.blogdemo.utils;

/**
 * Created by Vujica on 03-Mar-17.
 */

public class ApiError {
public final static int OK=200;
    public static final int MEDIA_HEADER_WRONG=406;
    public static final int WRONG_CONTENT_TYPE=415;
    public static final int BAD_REQUEST=400;
    public static final int WRONG_CREDENTIALS=401;
    public static final int SOMETHING_WENT_WRONG=1001;
    public static final int BLOG_NOT_EXISTING=404;
}
