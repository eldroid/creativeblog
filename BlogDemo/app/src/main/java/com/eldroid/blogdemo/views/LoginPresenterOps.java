package com.eldroid.blogdemo.views;

import android.widget.AutoCompleteTextView;
import android.widget.EditText;

/**
 * Created by Vujica on 02-Mar-17.
 */

public interface LoginPresenterOps {
    void loginUser(AutoCompleteTextView email, EditText password);

}
