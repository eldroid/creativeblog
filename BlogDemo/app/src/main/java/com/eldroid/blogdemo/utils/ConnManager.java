package com.eldroid.blogdemo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Vujica on 05-Mar-17.
 */

public class ConnManager {
    private static ConnManager connManager;
    private Context context;


    public static ConnManager getInstance(Context context)
    {
       if(connManager==null)
           connManager=new ConnManager(context);
        return connManager;
    }

    public boolean isConnected()
    {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public ConnManager(Context con)
    {
       context=con;

    }

}
