package com.eldroid.blogdemo.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.eldroid.blogdemo.R;
import com.eldroid.blogdemo.fragments.BaseFragment;
import com.eldroid.blogdemo.fragments.BlogFragment;

import java.util.Stack;


/**
 * Created by Vujica on 01-Mar-17.
 */

public abstract class BaseActivity extends AppCompatActivity {
    FragmentManager fragmentManager;
    protected Stack<BaseFragment> mStack;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResID());
        if(savedInstanceState!=null)
        {
            mStack=(Stack<BaseFragment>) savedInstanceState.getSerializable("fragments");
            if(mStack==null)
                mStack=new Stack<>();
        }
        else {
            mStack = new Stack<>();
        }
        fragmentManager=getSupportFragmentManager();
        linkUI();
        init();
        setData();
        setActions();

    }
    protected abstract int getLayoutResID();
    protected void init(){};
    protected void setData(){};
    protected  void linkUI(){};
    protected void setActions(){};
    protected void clearData(){};
    protected void setActionBarTitle(){};

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearData();
    }

    @Override
    public void onBackPressed() {
        if(getTopFragment()!=null)
        {
           removeTopFragment();
        }
        else if(!doubleBackToExitPressedOnce &&(this.getClass().getName().contains("MainActivity")))
        {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,getResources().getString(R.string.action_pressed_back), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
        else {
            try {
                super.onBackPressed();
            }
            catch (IllegalStateException e)
            {
                Log.d("error on back pressed:",e.getMessage());
            }
        }
    }
    public BaseFragment getTopFragment()
    {
        if(!mStack.isEmpty())
        {
            return mStack.peek();
        }
        return null;
    }
    public void removeTopFragment()
    {
        if(!mStack.isEmpty())
        {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            BaseFragment top = mStack.pop();
            fragmentTransaction.remove(top);
            fragmentTransaction.commit();
            if(mStack.isEmpty()) {
               setActionBarTitle();
            }
        }
    }
    public void addFragment(int layoutResId, BlogFragment fragment, String TAG)
    {
        if(fragment ==null)
            throw new RuntimeException("Invalid arguments fragment == null");
        if(layoutResId == 0)
            throw new RuntimeException("Invalid arguments layoutResId == 0");
        if(!mStack.isEmpty())
        {
            BaseFragment top = mStack.peek();
            if(!top.getClass().getCanonicalName().equals(fragment.getClass().getCanonicalName()))
            {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(layoutResId, fragment,TAG);
                fragmentTransaction.commit();
                mStack.add(fragment);
            }
        }
        else
        {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(layoutResId, fragment,TAG);
            fragmentTransaction.commit();
            mStack.add(fragment);
        }
    }
}
