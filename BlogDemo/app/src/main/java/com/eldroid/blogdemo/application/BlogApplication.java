package com.eldroid.blogdemo.application;

import android.app.Application;

import com.eldroid.blogdemo.API.BlogService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Vujica on 01-Mar-17.
 */

public class BlogApplication extends Application {

    private static Retrofit retrofit;
    private static String BASE_URL="http://blogsdemo.creitiveapps.com:16427";
    private static BlogService blogService;
    public  static final String PREFER_NAME="BLOG_PREF";
    public static  boolean HAS_NET;


    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        blogService = retrofit.create(BlogService.class);
    }

    public static boolean isConnected()
    {
        return HAS_NET;
    }

    public static BlogService getBLogService()
    {
        return blogService;
    }
}
