package com.eldroid.blogdemo.API;

import com.eldroid.blogdemo.models.Blog;
import com.eldroid.blogdemo.models.User;
import com.eldroid.blogdemo.responses.BlogResponse;
import com.eldroid.blogdemo.responses.LoginResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Vujica on 03-Mar-17.
 */

public interface BlogService {
    @Headers({
            "X-Client-Platform:Android",
            "Accept: application/json",
            "Content-Type: application/json"
    })

    @POST("/login")
    Call<LoginResponse> login(@Body User user);
    @Headers({
            "X-Client-Platform:Android",
            "Accept: application/json",})
    @GET("/blogs")
    Call<List<Blog>> getBlogList(@Header("X-Authorize") String token);

    @Headers({
            "X-Client-Platform:Android",
            "Accept: application/json",})
    @GET("/blogs/{id}")
    Call<BlogResponse> getBlog(@Path("id")int blogId, @Header("X-Authorize") String token);
}
