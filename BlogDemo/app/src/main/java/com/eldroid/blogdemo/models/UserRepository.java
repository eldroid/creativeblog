package com.eldroid.blogdemo.models;

import com.eldroid.blogdemo.utils.UserLoginCallback;

/**
 * Created by Vujica on 02-Mar-17.
 */

public interface UserRepository {
     public User getUser();
     public void login(String email, String pass, UserLoginCallback loginCallback);
     public void saveUserCredentials(String token);

}
