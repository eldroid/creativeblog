package com.eldroid.blogdemo.repositories;

import com.eldroid.blogdemo.utils.BlogCallback;
import com.eldroid.blogdemo.utils.BlogListCallback;

/**
 * Created by Vujica on 03-Mar-17.
 */

public interface BlogRepository {

    public void getBlogs(BlogListCallback callback);
    public void deleteToken();
    public void getBlog(int id, BlogCallback callback);
}
