package com.eldroid.blogdemo.responses;

import com.google.gson.annotations.Expose;

/**
 * Created by Vujica on 04-Mar-17.
 */

public class BlogResponse {
    @Expose
    private String content;
    public String getContent(){
        return content;
    }
    public void setContent(String con)
    {
        content=con;
    }
}
