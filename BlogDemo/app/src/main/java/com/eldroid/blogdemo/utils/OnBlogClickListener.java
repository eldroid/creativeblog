package com.eldroid.blogdemo.utils;

import com.eldroid.blogdemo.models.Blog;

/**
 * Created by Vujica on 03-Mar-17.
 */

public interface OnBlogClickListener {
    public void onBlogClicked(Blog blog);
}
