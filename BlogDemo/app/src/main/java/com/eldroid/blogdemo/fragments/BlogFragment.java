package com.eldroid.blogdemo.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.eldroid.blogdemo.R;
import com.eldroid.blogdemo.activities.MainActivity;
import com.eldroid.blogdemo.models.Blog;

/**
 * Created by Vujica on 01-Mar-17.
 */

public class BlogFragment extends BaseFragment{
    public static String TAG=BlogFragment.class.getSimpleName();
    private WebView wv_content;
    private Blog mBlog;
    private ProgressDialog progress;


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_blog;
    }

    public void linkUI()
    {
       wv_content=(WebView) mainActivity.findViewById(R.id.wv_blogContent);
    }
    public void init()
    {
        progress= ProgressDialog.show(mainActivity, "", "Loading...");
        WebSettings webSettings = wv_content.getSettings();
        webSettings.setJavaScriptEnabled(true);
        try {
            mBlog = (Blog) getArguments().getSerializable(Blog.KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        wv_content.setWebChromeClient(new ChromeClient());

    }
    public void setData()
    {
        if (mBlog!=null)
            wv_content.loadData(mBlog.getDescription(),"text/html; charset=utf-8", "UTF-8");
        if (mainActivity.getSupportActionBar()!=null)
            mainActivity.getSupportActionBar().setTitle("Blog Display");


    }
    public void setActions()
    {

    }

    private class ChromeClient extends WebChromeClient
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if(newProgress>=85)
            {
                if(progress!=null && progress.isShowing())
                    progress.dismiss();

            }
            super.onProgressChanged(view, newProgress);
        }
    }


}
