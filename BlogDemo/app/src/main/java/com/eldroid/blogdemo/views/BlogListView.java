package com.eldroid.blogdemo.views;

import android.content.Context;

import com.eldroid.blogdemo.models.Blog;

import java.util.List;

/**
 * Created by Vujica on 03-Mar-17.
 */

public  interface BlogListView  {
    public void displayBlogList(List<Blog> blogList);
    public void displayNoInternetError();
    public void handleError(int errorNum);
    public void logout();
    public void displayBlog(Blog blog);
    public Context getAppContext();

}
