package com.eldroid.blogdemo.utils;

import com.eldroid.blogdemo.models.Blog;

/**
 * Created by Vujica on 04-Mar-17.
 */

public interface BlogCallback {
    public void onSuccess(Blog blog);
    public void onError(int errorCode);
}
