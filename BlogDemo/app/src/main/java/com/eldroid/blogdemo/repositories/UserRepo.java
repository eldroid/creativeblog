package com.eldroid.blogdemo.repositories;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.eldroid.blogdemo.application.BlogApplication;
import com.eldroid.blogdemo.models.User;
import com.eldroid.blogdemo.models.UserBuilder;
import com.eldroid.blogdemo.models.UserRepository;
import com.eldroid.blogdemo.responses.LoginResponse;
import com.eldroid.blogdemo.utils.UserLoginCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Vujica on 03-Mar-17.
 */

public class UserRepo implements UserRepository {

    private Context context;
    private SharedPreferences sharedPreferences;
    public  UserRepo(Context ctx)
    {
        this.context=ctx;
        sharedPreferences=context.getSharedPreferences(BlogApplication.PREFER_NAME,1);
    }
    @Override
    public User getUser() {
       String token=sharedPreferences.getString("token",null);
        if(token!=null)
        {
            return new UserBuilder().setToken(token).setEmail("").createUser();
        }
        return null;
    }

    @Override
    public void login(final String email, String pass, final UserLoginCallback loginCallback) {
        User user=new User(email,null,pass);
       final Call<LoginResponse> loginCall=BlogApplication.getBLogService().login(user);
        loginCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse=response.body();
                if(loginResponse!=null)
                loginCallback.onLoginSuccess(loginResponse.getToken());
                else
                {
                    int error=response.code();
                    Log.d("UserLogin","Error login in. COde:"+error);
                    loginCallback.onLoginError(error);
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                    loginCallback.onLoginError(1001);
            }
        });

    }

    @Override
    public void saveUserCredentials(String token) {
            if(sharedPreferences!=null)
            {
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("token",token);
                editor.commit();
            }

    }
}
