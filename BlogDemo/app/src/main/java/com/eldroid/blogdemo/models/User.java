package com.eldroid.blogdemo.models;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Vujica on 02-Mar-17.
 */

public class User {
    @Expose
    private String email;
    @Expose
    private String token;
    @Expose
    private String password;

    public User(String email, String token,String password){
        this.email=email;
        this.token=token;
        this.password=password;
    }
    public String getToken(){
        return token;
    }
    private String getEmail(){return email;}
    private void setToken(String tkn)
    {
        token=tkn;
    }
}