package com.eldroid.blogdemo.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vujica on 03-Mar-17.
 */

public class LoginResponse implements Serializable{
    @SerializedName("token")
    @Expose
    private String token;

    public void setToken(String tok)
    {
        token=tok;
    }
    public String getToken()
    {
        return token;
    }
}
