package com.eldroid.blogdemo.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.eldroid.blogdemo.application.BlogApplication;
import com.eldroid.blogdemo.utils.ConnManager;

/**
 * Created by Vujica on 05-Mar-17.
 */

public class ConnectivityReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getExtras()!=null) {
            String action=intent.getAction();
            if(action!=null) {
                if(action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                    ConnectivityManager cm =
                            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null &&
                            activeNetwork.isConnectedOrConnecting();

                    if (isConnected) {
                        //todo send to all activities network change
                    } else {
                        //todo send to all activities network change
                    }
                }
            }
        }
    }

}
