package com.eldroid.blogdemo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eldroid.blogdemo.R;
import com.eldroid.blogdemo.models.Blog;
import com.eldroid.blogdemo.utils.OnBlogClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vujica on 03-Mar-17.
 */

public class BlogListAdapter extends RecyclerView.Adapter<BlogListAdapter.BlogViewHolder>  {

    private ArrayList<Blog> blogArrayList;
    private Context context;
    private OnBlogClickListener blogClickListener;
    public BlogListAdapter(Context context, ArrayList<Blog>blogList, OnBlogClickListener clickListener)
    {
        this.context=context;
        blogArrayList=blogList;
        blogClickListener=clickListener;
    }

    @Override
    public BlogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_item, parent, false);
        return new BlogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BlogViewHolder holder, int position) {
            final Blog blog=blogArrayList.get(position);
            holder.txt_title.setText(blog.getTitle());
            holder.txt_desciption.setText(Html.fromHtml(blog.getDescription()));
            Picasso.with(context).load(blog.getImageUrl()).placeholder(context.getResources().getDrawable(R.mipmap.image_placeholder)).into(holder.img_cover);
            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    blogClickListener.onBlogClicked(blog);
                }
            });
    }

    @Override
    public int getItemCount() {
        return blogArrayList.size();
    }

    public class  BlogViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layout;
        private ImageView img_cover;
        private TextView txt_title;
        private TextView txt_desciption;

        public BlogViewHolder(View itemView) {
            super(itemView);
            layout=(LinearLayout)itemView.findViewById(R.id.lay_blog_item_wrapper);
            img_cover=(ImageView)itemView.findViewById(R.id.img_cover);
            txt_desciption=(TextView)itemView.findViewById(R.id.txt_desciption);
            txt_title=(TextView)itemView.findViewById(R.id.txt_title);
        }
    }
    public void clear()
    {
        blogArrayList.clear();
        notifyDataSetChanged();
    }
    public void addAll(ArrayList<Blog>blogs)
    {
        blogArrayList.addAll(blogs);
        notifyDataSetChanged();
    }
}
