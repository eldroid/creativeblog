package com.eldroid.blogdemo.utils;

/**
 * Created by Vujica on 02-Mar-17.
 */

public interface UserLoginCallback {
    public void onLoginSuccess(String token);
    public void onLoginError(int error);
}
